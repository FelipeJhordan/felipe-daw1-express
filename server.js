const express = require('express')
const app = express()
var path = require('path');

app.use(express.static(path.join(__dirname, 'public')))
app.use(require("./app/routes.js"))

app.listen(3000)