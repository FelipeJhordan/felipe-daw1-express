const dirFormated =  __dirname.replace("app","") + "\\views\\"

const stringUtils = {
        dirViewIndex: dirFormated + "index.html",
        dirViewContato: dirFormated + "contato.html",
        dirViewSobre: dirFormated + "sobre.html"
}

const mountMessage = (nome, mensagem, email) => {
    return `Obrigado ${nome} por ter enviado a mensagem ${mensagem}. ` +
        `Retornaremos no e-mail ${email}.`
}


module.exports = { 
    stringUtils,
    mountMessage
}