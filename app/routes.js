var router = require("express").Router()
var { stringUtils, mountMessage } = require("./utils");



router.get('/', function (req, res) {
    res.sendFile(stringUtils.dirViewIndex)
})

router.get('/contato', function (req, res) {
    res.sendFile(stringUtils.dirViewContato)
})

router.get('/sobre', function (req, res) {
    res.sendFile(stringUtils.dirViewSobre)
})

router.get('/confirmacao', function (req, res) {
    let formData = req.query
    res.send(mountMessage(formData.nome, formData.mensagem, formData.email) + "<br/> <a href='/'> Voltar </a>")
})

router.get('*', function (req, res) {
    res.redirect("/")
})

// TODO eu vi que é uma maneira de identificar o erro 404  NotFound ( quando usuário tenta acessar um link que não existe)
module.exports = router